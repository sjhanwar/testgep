\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}About GEP}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Quick-Start}{2}{section.1.2}
\contentsline {chapter}{\numberline {2}Installation}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Dependencies}{6}{section.2.1}
\contentsline {section}{\numberline {2.2}Installation}{6}{section.2.2}
\contentsline {chapter}{\numberline {3}GEP functionality}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Daemons and how they works together}{8}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Daemons}{8}{subsection.3.1.1}
\contentsline {subsubsection}{Arbiter}{8}{subsubsection*.3}
\contentsline {subsubsection}{Scheduler}{8}{subsubsection*.4}
\contentsline {subsubsection}{Poller}{8}{subsubsection*.5}
\contentsline {subsubsection}{Receiver}{8}{subsubsection*.6}
\contentsline {subsubsection}{Broker}{9}{subsubsection*.7}
\contentsline {subsubsection}{Reactionner}{9}{subsubsection*.8}
\contentsline {section}{\numberline {3.2}Usage}{9}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Feature selection}{9}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Predict}{10}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Train and Predict GEP}{10}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Train and Predict SVM}{11}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}n-fold cross-validation}{12}{subsection.3.2.5}
\contentsline {subsection}{\numberline {3.2.6}Prepare genomewide prediction}{13}{subsection.3.2.6}
\contentsline {subsection}{\numberline {3.2.7}Prepare training}{13}{subsection.3.2.7}
\contentsline {subsubsection}{With sources and pip}{14}{subsubsection*.9}
\contentsline {section}{\numberline {3.3}Predict only}{15}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}OutPut}{15}{subsection.3.3.1}
\contentsline {section}{\numberline {3.4}Train and Predict}{15}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Train}{15}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Predict}{16}{subsection.3.4.2}
\contentsline {section}{\numberline {3.5}nFold cross-validation}{16}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Usage}{16}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Output}{16}{subsection.3.5.2}
\contentsline {section}{\numberline {3.6}Enrichment analysis}{16}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Overlap with enhancers}{17}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}Enrichment test}{17}{subsection.3.6.2}
\contentsline {subsection}{\numberline {3.6.3}Output}{17}{subsection.3.6.3}
\contentsline {section}{\numberline {3.7}Accessary programs}{17}{section.3.7}
\contentsline {subsection}{\numberline {3.7.1}Output}{18}{subsection.3.7.1}
\contentsline {chapter}{\numberline {4}Configuration}{19}{chapter.4}
\contentsline {section}{\numberline {4.1}Arbiter}{20}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Monitoring resources configuration}{20}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}External modules configuration}{20}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Monitoring objects configuration}{20}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Daemons configuration}{21}{subsection.4.1.4}
\contentsline {subsubsection}{Arbiter configuration}{21}{subsubsection*.10}
\contentsline {paragraph}{Definition Format}{21}{paragraph*.11}
\contentsline {paragraph}{Example Definition:}{21}{paragraph*.12}
\contentsline {paragraph}{Variable Descriptions}{22}{paragraph*.13}
\contentsline {subsubsection}{Scheduler configuration}{22}{subsubsection*.14}
\contentsline {paragraph}{Definition Format}{22}{paragraph*.15}
\contentsline {paragraph}{Example Definition:}{23}{paragraph*.16}
\contentsline {paragraph}{Variable Descriptions}{24}{paragraph*.17}
\contentsline {subsubsection}{Broker configuration}{24}{subsubsection*.18}
\contentsline {paragraph}{Definition Format}{24}{paragraph*.19}
\contentsline {paragraph}{Example Definition:}{24}{paragraph*.20}
\contentsline {paragraph}{Variable Descriptions}{25}{paragraph*.21}
\contentsline {subsubsection}{Poller configuration}{25}{subsubsection*.22}
\contentsline {paragraph}{Definition Format}{25}{paragraph*.23}
\contentsline {paragraph}{Example Definition:}{26}{paragraph*.24}
\contentsline {paragraph}{Variable Descriptions}{26}{paragraph*.25}
\contentsline {subsubsection}{Reactionner configuration}{27}{subsubsection*.26}
\contentsline {paragraph}{Definition Format}{27}{paragraph*.27}
\contentsline {paragraph}{Example Definition:}{28}{paragraph*.28}
\contentsline {paragraph}{Variable Descriptions}{28}{paragraph*.29}
\contentsline {subsubsection}{Reaceiver configuration}{28}{subsubsection*.30}
\contentsline {paragraph}{Definition Format}{28}{paragraph*.31}
\contentsline {paragraph}{Example Definition:}{29}{paragraph*.32}
\contentsline {paragraph}{Variable Descriptions}{29}{paragraph*.33}
\contentsline {section}{\numberline {4.2}Daemons}{29}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Ini files}{29}{subsection.4.2.1}
\contentsline {subsubsection}{Scheduler}{30}{subsubsection*.34}
\contentsline {subsubsection}{Poller}{30}{subsubsection*.35}
\contentsline {subsubsection}{Receiver}{31}{subsubsection*.36}
\contentsline {subsubsection}{Broker}{31}{subsubsection*.37}
\contentsline {subsubsection}{Reactionner}{32}{subsubsection*.38}
\contentsline {section}{\numberline {4.3}Modules}{32}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Alignak modules (official way)}{32}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Shinken modules}{32}{subsection.4.3.2}
\contentsline {chapter}{\numberline {5}How to contribute}{33}{chapter.5}
\contentsline {section}{\numberline {5.1}Links to know}{34}{section.5.1}
\contentsline {section}{\numberline {5.2}Getting Help and Ways to Contribute}{34}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Setup environment for Alignak}{34}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Getting started into the developer documentation}{34}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Git and GitHub 101}{34}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Bug report}{35}{subsection.5.2.4}
\contentsline {subsection}{\numberline {5.2.5}Step by step contribution example :}{35}{subsection.5.2.5}
\contentsline {subsubsection}{Simple fix}{35}{subsubsection*.39}
\contentsline {subsubsection}{Not so simple fix}{36}{subsubsection*.40}
\contentsline {paragraph}{Checkout new branch}{36}{paragraph*.41}
\contentsline {paragraph}{Run tests}{36}{paragraph*.42}
\contentsline {paragraph}{Commit}{36}{paragraph*.43}
\contentsline {paragraph}{Create new tests}{36}{paragraph*.44}
\contentsline {paragraph}{Create pull request}{37}{paragraph*.45}
\contentsline {subsection}{\numberline {5.2.6}Release TODO list :}{38}{subsection.5.2.6}
