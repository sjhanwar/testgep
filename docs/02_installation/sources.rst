.. _Installation/sources:

============
Dependencies
============

For successful implementation of the software, please make sure that your system has following:

* Software

    * Bedtools: The tool assumes bedtools in the path. Else You can download the latest version of the bedtools from page: https://code.google.com/p/bedtools/downloads/list. After installation, please make sure bedtools is in the path.

* Python modules:

    * Scipy: visit http://www.scipy.org/install.html
    * Matplotlib: visit http://matplotlib.org/users/installing.html
    * Numpy: visit http://docs.scipy.org/doc/numpy/user/install.html
    * Scikit-learn: visit http://scikit-learn.org/stable/install.html

* Perl modules:

    * CWD: visit http://search.cpan.org/~rjbs/PathTools-3.59/Cwd.pm
    * Math::Round visit http://search.cpan.org/dist/Math-Round/Round.pm
    * Or install them from CPAN shell: http://www.twiki.org/cgi-bin/view/TWiki/HowToInstallCpanModules

* R library:

    * GenomicRanges: visit https://bioconductor.org/packages/release/bioc/html/GenomicRanges.html

============
Installation
============

1. GitHub: source archive on this page: https://github.com/Alignak-monitoring/alignak/releases

    Run in a terminal: git colne <http link>

2. Download from <ftp.//………/GEP.tar.gz>

    Download it and uncompressed the code as: tar –xvzf GEP.tar.gz