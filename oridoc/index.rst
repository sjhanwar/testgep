.. Alignak documentation master file, created by
   sphinx-quickstart on Wed Nov 13 01:01:23 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

===================================
Welcome to GEP documentation!
===================================

.. toctree::
    :maxdepth: 2

    ../source/01_introduction/index
    ../source/02_installation/index
    ../source/03_how_it_work/index
    ../source/04_configuration/index

